-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2019 at 09:08 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_vue`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `mobile`, `created_at`, `updated_at`) VALUES
(1, 'sayaf', 'bdsayaf@gmail.com', '01643038855', '2019-08-29 08:58:11', '2019-08-29 08:58:11'),
(2, 'emon', 'emon435@gmail.com', '01664565421321', '2019-08-29 08:58:15', '2019-08-29 08:58:15'),
(3, 'sifaat', 'sifat@gmail.com', '01643038855', '2019-08-29 08:58:11', '2019-08-29 08:58:11'),
(4, 'emon', 'emon435@gmail.com', '01664565421321', '2019-08-29 08:58:15', '2019-08-29 08:58:15'),
(6, 'emon', 'emon435@gmaasdasdil.com', '123123123', '2019-08-29 08:58:15', '2019-08-29 06:33:03'),
(12, 'sayaf', 'bdsayaf@gmail.com', '01746304668', '2019-08-29 06:20:51', '2019-08-30 13:03:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_29_084407_create_contacts_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(7, '2016_06_01_000004_create_oauth_clients_table', 2),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('020c60664a3d881c8b2770406743e24be7ddc99acf290ce6b1809455a2cdce790b4715f0d7c9956c', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:17:06', '2019-08-30 10:17:06', '2020-08-30 16:17:06'),
('05d7db6126aa9b40d1cffda0f0a509960e74364b12b70511bc82504255a91a10c49f170e5c78af4c', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:25:13', '2019-08-30 12:25:13', '2020-08-30 18:25:13'),
('0620f2da8c1988c430ee422fa84d3e6da0decf82455c308993c4618f3add30d0913822b2eb853130', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:31:35', '2019-08-30 10:31:35', '2020-08-30 16:31:35'),
('0ad58a7f7d274b275ba487f0a2a8adc5db180cf571850934235b9173060c8b5f3846f7d0576bdc16', 2, 1, 'TutsForWeb', '[]', 1, '2019-08-30 07:33:08', '2019-08-30 07:33:08', '2020-08-30 13:33:08'),
('174b9dd09a1bf9d874018c7c2e69142f260f30ad6c1b1808313c1453b3421860cdb5cb03f1e48b04', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 11:51:01', '2019-08-30 11:51:01', '2020-08-30 17:51:01'),
('185b6c51ed2215085b362d55aede252cc7c6e885353aedbac76fc8f846e6b8af734449ac1af7fd70', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:17:15', '2019-08-30 12:17:15', '2020-08-30 18:17:15'),
('18f84ed69fa9c68877277300f7ea250218cee2ae45306adc00b8fb83b3edf872d0d9f3dcbb0befc1', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:55:12', '2019-08-30 10:55:12', '2020-08-30 16:55:12'),
('1ddd4c86547234cc6ae37cbccf759de84c17f53bda2ba6bb416cb68182e55096827543346e357f6a', 5, 1, 'TutsForWeb', '[]', 0, '2019-08-30 13:05:39', '2019-08-30 13:05:39', '2020-08-30 19:05:39'),
('295bf82bdd494caceec45d598bb06ef6b67df3a4fc864c9f84a8a7451feac22519477d2aa6e604d4', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:55:43', '2019-08-30 10:55:43', '2020-08-30 16:55:43'),
('348219d8f4200dbb686d6197b00cc3d18c5de5ccaea958a291ea41c30a3e8c9de0b7b6c8bb95997a', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:29:34', '2019-08-30 12:29:34', '2020-08-30 18:29:34'),
('3abbc298eed8acbe2f4b1d3dd4866b9460ab3b1122736073e66461c4d3ab05de8a836084515597a6', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:53:31', '2019-08-30 10:53:31', '2020-08-30 16:53:31'),
('3b2a465f17547533b136bf2604d43a774c32fc3d08e694791d6a6fe1a6bf0396c50e74a5c4b5b29b', 1, 1, 'TutsForWeb', '[]', 0, '2019-08-30 07:29:39', '2019-08-30 07:29:39', '2020-08-30 13:29:39'),
('44df8555ff3a655809c561793c5958ab5668641dc88a59109dfb501b2f506f03e9762e86e76cd5d0', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 11:51:57', '2019-08-30 11:51:57', '2020-08-30 17:51:57'),
('450fd67cdb50e78ef01315e1cfaaa8fe73f488bd7126ef386351cbeaee618711999c0543b992ce00', 5, 1, 'TutsForWeb', '[]', 0, '2019-08-30 13:01:28', '2019-08-30 13:01:28', '2020-08-30 19:01:28'),
('5202c17a2945baf62d16cd88a966882f5627256ef5bdcc40df0ffe475706c9c6d6de431f9c534f04', 2, 1, 'TutsForWeb', '[]', 1, '2019-08-30 11:23:49', '2019-08-30 11:23:49', '2020-08-30 17:23:49'),
('53fd13175189f530133a2a53c8f60467bfe8947f1bdd164bef15718397b611959846a6b5aecacea9', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:54:52', '2019-08-30 10:54:52', '2020-08-30 16:54:52'),
('55e5c121c7260092c69849b318575df40ac18e51f8d202427f02a83dceeed6870906ff0a9ec4cfcf', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:55:30', '2019-08-30 10:55:30', '2020-08-30 16:55:30'),
('57be324e097c4a8571b7d6d96a8996c00b11ed37f23759d7584187001e85f76f970717a89333af4e', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:19:03', '2019-08-30 12:19:03', '2020-08-30 18:19:03'),
('58a008dde805b4781e09c7344a34ad5da40cf0500d5bd01daaffd7d9185915b9efdcd0b2b7781d28', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 11:49:24', '2019-08-30 11:49:24', '2020-08-30 17:49:24'),
('5ce59a468d917814de5bafe8b17a63aaed10eaf94ea385d38593abcf5af280b2c1729552e6877c1f', 4, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:58:48', '2019-08-30 12:58:48', '2020-08-30 18:58:48'),
('5dcb0907699932bb215d25f6a8d9091ab53029722e5994d0829d1be0bd1abfa6338496053c7ff9a4', 5, 1, 'TutsForWeb', '[]', 0, '2019-08-30 13:03:22', '2019-08-30 13:03:22', '2020-08-30 19:03:22'),
('5f3fc0a6a9cecf311a3b64a0c9e2c1a4faf90cfa0d2d931a75854b656dfd0f7207ae44a3bcd8d4bf', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:20:46', '2019-08-30 12:20:46', '2020-08-30 18:20:46'),
('61978ab7fdc8d90bb04804d3d325f9080dbe5c8c1cb8fa1e85f87d06b66acbe76a5ed1d9a1497723', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 07:31:16', '2019-08-30 07:31:16', '2020-08-30 13:31:16'),
('6873f1d604abcff960fb0cb773468784cc39aebf6d3f19dc25224c198fc1f04313616e8b06d2b983', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:17:25', '2019-08-30 10:17:25', '2020-08-30 16:17:25'),
('6b3bddb7a5b06973e6eb37d14b61f7feeb89ed4a4a5bb463292adcb9b24017c69895d544fb16b6f8', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:20:58', '2019-08-30 12:20:58', '2020-08-30 18:20:58'),
('7251603c6db048e27452834681ebccbced202d0b31bf07d3e5f75d5e217474edfaa32f57b220ffc3', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 09:46:44', '2019-08-30 09:46:44', '2020-08-30 15:46:44'),
('7888a71413ae0f98e8437a3e8e39a9dbc0da78aca7f59fe81e5a9c9e1ebff279c665c1eda8770900', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:25:42', '2019-08-30 12:25:42', '2020-08-30 18:25:42'),
('81c3537f01231ec831e537b3d94d1fabf46eeaa822f47f4bb6be0bebcc98c8e6c1c95bd97b8b19fa', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:21:58', '2019-08-30 12:21:58', '2020-08-30 18:21:58'),
('8380128126fb1426016adddebc8bcdfa2ce5c81e5cb7b58cd0e10865c2dd1342dbf2da4cdfb1c007', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:29:32', '2019-08-30 10:29:32', '2020-08-30 16:29:32'),
('87c760e81d0ab17df52623faba8c9f143bc119dbdfb89e5be28cff920316d6a2d14080e63a90312c', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 11:53:30', '2019-08-30 11:53:30', '2020-08-30 17:53:30'),
('8944d3bbd773de04377ec5e99abf40cf8d5da43217af0955cb70149cc5b9f25394fbce78c49788ec', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 07:32:20', '2019-08-30 07:32:20', '2020-08-30 13:32:20'),
('8db198fab50fb50de5d7dbc631b6ea41986c3a81dece6ddefb1e3336d2dfa8f9827efd33f2b0f1ce', 5, 1, 'TutsForWeb', '[]', 0, '2019-08-30 13:02:14', '2019-08-30 13:02:14', '2020-08-30 19:02:14'),
('8f39393672470a0315da87b2e0133e6586d01715405ba36daba4303bb325683b9ebdb2aaefbfe480', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:15:48', '2019-08-30 12:15:48', '2020-08-30 18:15:48'),
('92f8bddbf7b8b7330bd80b642b9b2df43ccdda864a6abf38036a71238028445995dba25aaf25d29f', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:19:37', '2019-08-30 12:19:37', '2020-08-30 18:19:37'),
('93c02421e11f9b30ed05296edabe4f6405d19e811f1ec9a51f2468cf8dc731c9c6cb185ca6edcff6', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:16:19', '2019-08-30 12:16:19', '2020-08-30 18:16:19'),
('9b78fc4cc300f25b8f5781123167ac70e7200e7ea4b576eff41880b0f12b3b206ed7e79e566f5f58', 2, 1, 'TutsForWeb', '[]', 1, '2019-08-30 11:06:24', '2019-08-30 11:06:24', '2020-08-30 17:06:24'),
('a29e3e75d11ec91948604821ef7100f1fd4fd6105c8c1514f0815868b2a0c27ca407d3033835d066', 5, 1, 'TutsForWeb', '[]', 0, '2019-08-30 13:02:54', '2019-08-30 13:02:54', '2020-08-30 19:02:54'),
('a354e462422bbe5c96467a528404478a9485ba4563acb6ec75759095e50b2a4e6d4fe6a637f7d9cf', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 07:32:08', '2019-08-30 07:32:08', '2020-08-30 13:32:08'),
('af610434d8e1ad18cb702ea9226f05b5e98e557074730cf16d5d7caadee15801718d62fb5aaae700', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:53:06', '2019-08-30 10:53:06', '2020-08-30 16:53:06'),
('af665a0a9823c89460b57a9498c6fbf3a3d8604f42758fb90499dffa93273615ff8f3204892a0b33', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:27:25', '2019-08-30 12:27:25', '2020-08-30 18:27:25'),
('b540eb9ad56b29098a4a05b8473600d5d04a005b19fc6cd510df379684770c0948fc238bc6cf75b2', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 11:28:09', '2019-08-30 11:28:09', '2020-08-30 17:28:09'),
('b5cac23d6708b1d53dc727c61b2571e33f690720af05d34363b6aa4f9ed9dab89f7181431b7370a0', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:27:35', '2019-08-30 10:27:35', '2020-08-30 16:27:35'),
('b8c79762f7003aabd8558a98c7bd5c51dca260fa01d935623225875b4a510759dc47af929d66a45b', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:48:46', '2019-08-30 10:48:46', '2020-08-30 16:48:46'),
('bb8af4d8368a7170bfdeb7b7a910254489f128f008f310db0a247889828d67e53e5ce5d903e92ae6', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 11:39:33', '2019-08-30 11:39:33', '2020-08-30 17:39:33'),
('c2e87663fd2be16e425eeba017cbb9a06c56215f30b34f99231fc46a2a81192d753c3580f687089a', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:59:28', '2019-08-30 12:59:28', '2020-08-30 18:59:28'),
('c8427d41d52d98a66bb8743fb6780ac37c1faad7a104befa09477d6fe1b82c4bb3409bf6e08569fe', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:19:43', '2019-08-30 12:19:43', '2020-08-30 18:19:43'),
('ca8ad4723e528e9b7a3e7278e006bcc3677315a2c89eb8de8c1330be1cf76e1fcf4fe5a26444ba45', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:25:34', '2019-08-30 10:25:34', '2020-08-30 16:25:34'),
('cb6d2554ba84c57e36931e1feaefd4e6610aa645bf9ad4e32f6481f73c307c47777f1f19b2adfe23', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 11:03:22', '2019-08-30 11:03:22', '2020-08-30 17:03:22'),
('ddfa34a29c6243ceae00c1abe24ed24fa63583931d024f60a07c67eaea23a527e44de28839367c99', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 11:43:23', '2019-08-30 11:43:23', '2020-08-30 17:43:23'),
('dfad17eff2840d9e33b20e6cdcf927ecf1003054fdb45eb528d96682c937deb3ed4f225813c39f78', 5, 1, 'TutsForWeb', '[]', 0, '2019-08-30 13:02:31', '2019-08-30 13:02:31', '2020-08-30 19:02:31'),
('e112a970dba922b606bece3640292d051f0e643ba36efc2dbca445a56d33e35cde0da8597692d702', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 10:06:21', '2019-08-30 10:06:21', '2020-08-30 16:06:21'),
('e1ba228c43529cd6b4ec6dec402b5b37e0f1f180800d4d1284f1c90b72587f58106a784a2554aeda', 5, 1, 'TutsForWeb', '[]', 0, '2019-08-30 13:07:15', '2019-08-30 13:07:15', '2020-08-30 19:07:15'),
('f730e676e1e9adb4acd01c6144bc39d1ed9e5877afeef24c68316a73a7d6c9297f68e90ef274bca7', 2, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:27:32', '2019-08-30 12:27:32', '2020-08-30 18:27:32'),
('ff730428ce38c5c23c51f60172c7d6d054d7df43b2086db1498e4375c7895be0fcb534376a52d8bc', 3, 1, 'TutsForWeb', '[]', 0, '2019-08-30 12:56:16', '2019-08-30 12:56:16', '2020-08-30 18:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '8TeRCkQOJi0r3H0QRtxw9XDi2D54yhy96n3O7ts5', 'http://localhost', 1, 0, 0, '2019-08-30 06:57:50', '2019-08-30 06:57:50'),
(2, NULL, 'Laravel Password Grant Client', 'ggZBkVGyMTrYUQI9kiAAxfg5tTtOkc8pixpUBBY9', 'http://localhost', 0, 1, 0, '2019-08-30 06:57:50', '2019-08-30 06:57:50');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-08-30 06:57:50', '2019-08-30 06:57:50');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sayaf', 'bdsayaf@gmail.com', NULL, '$2y$10$NWDllv6EbK48kJsQPzIMIe25tbmCjAn1BrFIUHlGRQjoZz/PYK8pO', NULL, '2019-08-30 07:29:39', '2019-08-30 07:29:39'),
(2, 'sayaf', 'sayaff@gmail.com', NULL, '$2y$10$meY9mTFEW9eM50vu6VAtWe.Httl9yhnpR9UYgopYohTOrnwWPcHqi', NULL, '2019-08-30 07:31:15', '2019-08-30 07:31:15'),
(3, 'sayaf', 'jahan@gmail.com', NULL, '$2y$10$5zFihSX.0WAPYyyG.jkOUujRbJc67dWdJZgnABLsE9ScVb25njaNK', NULL, '2019-08-30 12:56:16', '2019-08-30 12:56:16'),
(4, 'asddf', 'jasdsan@gmail.com', NULL, '$2y$10$2dxmi6cprMYbv/EeS/3UbeBHfUC51dK4Ob9nLUMb0lRdh1D5TfL.q', NULL, '2019-08-30 12:58:48', '2019-08-30 12:58:48'),
(5, 'fajil', 'fajil@gmail.com', NULL, '$2y$10$pP3bK.Z28xN5qhfMFQTQwuyI37u4Td2GA6h7g2Uw9l3xOgFeSUYIy', NULL, '2019-08-30 13:01:28', '2019-08-30 13:01:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

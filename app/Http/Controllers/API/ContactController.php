<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::orderBy('id', 'desc')->paginate(5);
        return response()->json(['success' => true, 'contacts'=> $contacts]);
    }

    public function store(Request $request)
    {
        $contact = $request->isMethod('put') ? Contact::findOrFail($request->contact_id) : new Contact;
        $contact->id = $request->input('contact_id');
        $contact->name = $request->input('name');
        $contact->email = $request->input('email');
        $contact->mobile = $request->input('mobile');

        if($contact->save()) {
            return response()->json(['success' => true, 'Contact added successfull']);
        }
        
    }

    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        return response()->json(['success' => true, 'Delete successfull']);
    }
}

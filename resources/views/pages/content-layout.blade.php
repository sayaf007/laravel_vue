<!-- Description -->
@extends('layouts/contentLayoutMaster')

@section('title', 'Contact')

@section('content')

<section id="app">

   
    <contacts></contacts>
</section>
<!--/ Description -->
@endsection

@section('myscript')
<script src="{{ asset(mix('js/app.js')) }}" type="text/javascript"></script>
@endsection
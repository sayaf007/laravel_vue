<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('contacts', 'API\ContactController@index');

Route::post('contact', 'API\ContactController@store');

Route::put('contact', 'API\ContactController@store');

Route::delete('contact/{id}', 'API\ContactController@destroy');

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
    Route::get('logout', 'API\AuthController@logout')->name('logout');

});

Route::post('login', 'API\AuthController@login');
Route::post('register', 'API\AuthController@register');

